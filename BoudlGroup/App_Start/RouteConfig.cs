﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BoudlGroup
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            

            routes.MapRoute(
              name: "NewsGroup",              
              url: "News/{name}",
              defaults: new { controller = "Home", action = "News" },
              namespaces: new[] { "BoudlGroup.Controllers" }
          );
            routes.MapRoute(
              name: "NewsGroupArglobal",
              url: "ar/News/{name}",
              defaults: new { Areas = "ar", Controller = "Home", Action = "News" },
              namespaces: new[] { "BoudlGroup.Areas.ar.Controllers" }
          );
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "BoudlGroup.Controllers" }
            );
        }
    }
}
