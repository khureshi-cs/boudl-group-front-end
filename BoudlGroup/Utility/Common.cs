﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;

namespace BoudlGroup.Utility
{
    public class Common
    {
        public static bool sendEmail(string mailfrom, string mailTo, string strHTML, string ClientHTML, string Subject, string mailBCC)
        {
            try
            {
                string msg1 = string.Empty;
                MailMessage mMailMessage = new MailMessage();
                mMailMessage.From = new MailAddress(mailfrom);
                List<string> ToAddress = new List<string>();

                ToAddress.Add(System.Configuration.ConfigurationManager.AppSettings["admin"].ToString());
                ToAddress.Add(mailTo);
                foreach (var toAdr in ToAddress)
                {
                    if (toAdr != System.Configuration.ConfigurationManager.AppSettings["admin"].ToString())
                    {
                        mMailMessage.To.Add(new MailAddress(toAdr));
                        if (!string.IsNullOrWhiteSpace(mailBCC))
                            mMailMessage.Bcc.Add(mailBCC);

                        mMailMessage.Subject = Subject;
                        mMailMessage.Body = ClientHTML;
                        mMailMessage.IsBodyHtml = true;
                        mMailMessage.Priority = MailPriority.Normal;

                        SmtpClient smtp = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["SMTPServer"], Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["smtpPort"]));

                        smtp.UseDefaultCredentials = false;
                        smtp.Credentials = new System.Net.NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["mailFrom"].ToString(), System.Configuration.ConfigurationManager.AppSettings["mailCredentialPassword"].ToString());
                        
                        smtp.Send(mMailMessage);
                    }
                    else
                    {
                        mMailMessage.To.Add(new MailAddress(toAdr));
                        if (!string.IsNullOrWhiteSpace(mailBCC))
                            mMailMessage.Bcc.Add(mailBCC);

                        mMailMessage.Subject = Subject;
                        mMailMessage.Body = strHTML;
                        mMailMessage.IsBodyHtml = true;
                        mMailMessage.Priority = MailPriority.Normal;

                        SmtpClient smtp = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["SMTPServer"], Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["smtpPort"]));
                        smtp.UseDefaultCredentials = false;
                        smtp.Credentials = new System.Net.NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["mailFrom"].ToString(), System.Configuration.ConfigurationManager.AppSettings["mailCredentialPassword"].ToString());
                        smtp.Send(mMailMessage);
                    }

                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}