﻿using Boudl.Client.Mvc;
using BoudlGroup.Header;
using BoudlGroup.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace BoudlGroup.Areas.ar.Controllers
{
    public class FindAHotelController : Controller
    {
        // GET: ar/FindAHotel
        //public ActionResult Index()
        //{
        //    return View();
        //}
        public static Int64 HtId = 0;
        public static string FixedAmenitiesIds = "";
        public static string FixedBrandIds = "";

        public async Task<ActionResult> Index()
        {
            FindAHotelDTO obja = new FindAHotelDTO();
            using (HttpClient client = new HttpClient())
            {
                CommonHeader.setHeaders(client);
                ManageHotelAmenitiesDTO aObj = new ManageHotelAmenitiesDTO();
                FindAHotelNewDTO obj = new FindAHotelNewDTO();
                List<ManageCountriesDTO> CountriesList = new List<ManageCountriesDTO>();
                List<ManageCitiesDTO> Cityist = new List<ManageCitiesDTO>();
                //List<ManageBranchesDTO> BranchList = new List<ManageBranchesDTO>();
                string y = null;
                if (TempData["htlId"] != null)
                    ViewData["HotelId"] = TempData["htlId"];
                if (TempData["AmtId"] != null)
                    ViewData["AmenityId"] = TempData["AmtId"];
                List<ManageHotelAmenitiesDTO> a = new List<ManageHotelAmenitiesDTO>();
                ManageCountriesDTO CountryObj = new ManageCountriesDTO();
                ManageCitiesDTO cityObj = new ManageCitiesDTO();
                List<ManageCountriesDTO> CountryList = new List<ManageCountriesDTO>();
                List<ManageCitiesDTO> CityList = new List<ManageCitiesDTO>();
                List<ManageBranchesDTO> BranchList = new List<ManageBranchesDTO>();
                HttpResponseMessage AmenitiesRes = await client.PostAsJsonAsync("api/HotelAmenitiesAPI/NewGetHotelAmenity", aObj);
                if (AmenitiesRes.IsSuccessStatusCode)
                {
                    var AmenitiesData = AmenitiesRes.Content.ReadAsStringAsync().Result;
                    var AmenitiesList = JsonConvert.DeserializeObject<List<ManageHotelAmenitiesDTO>>(AmenitiesData);
                    a = AmenitiesList;
                    List<ManageHotelAmenitiesDTO> AmnList = AmenitiesList;
                    SelectList objModelData = new SelectList(AmnList, "Id", "NameAr", 0);
                    ViewBag.HotelAmenities = objModelData;
                }
                //HttpResponseMessage CountryRes = await client.PostAsJsonAsync("api/CountriesAPI/NewCountry", CountryObj);
                //if (CountryRes.IsSuccessStatusCode)
                //{
                //    var CountryData = CountryRes.Content.ReadAsStringAsync().Result;
                //    var ContryList = JsonConvert.DeserializeObject<List<ManageCountriesDTO>>(CountryData);
                //    SelectList ObjContData = new SelectList(ContryList, "Id", "NameAr", 0);
                //    ViewBag.CoutrList = ObjContData;
                //}

                HttpResponseMessage AmenRes = await client.PostAsJsonAsync("api/FindAHotelAPI/NewMapData", obj);
                if (AmenRes.IsSuccessStatusCode)
                {
                    var resData = AmenRes.Content.ReadAsStringAsync().Result;
                    var res = JsonConvert.DeserializeObject<ManageBranchesDTO>(resData);
                    var data = res.datasetXml;
                    if (data != null)
                    {
                        var document = new XmlDocument();
                        document.LoadXml(data);
                        DataSet ds = new DataSet();
                        ds.ReadXml(new XmlNodeReader(document));
                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[3].Rows.Count > 0)
                            {
                                CountriesList = ds.Tables[3].AsEnumerable().Select(dataRow => new ManageCountriesDTO
                                {
                                    Id = Convert.ToInt64(dataRow.Field<Int64>("CityId")),
                                    NameEn = dataRow.Field<string>("NameEn"),
                                    NameAr = dataRow.Field<string>("NameAr")
                                }).ToList();
                                    SelectList ObjContData = new SelectList(CountriesList, "Id", "NameAr", 0);
                                    ViewBag.CoutrList = ObjContData;
                                //TempData["CountriesList"] = CountriesList;
                            }
                            else
                            {
                                SelectList ObjContData = new SelectList("", "Id", "NameAr", 0);
                                ViewBag.CoutrList = ObjContData;
                            }
                            
                            if (ds.Tables[4].Rows.Count > 0)
                            {
                                var hotels = ds.Tables[4].AsEnumerable().Select(dataRow => new ManageHotelsDTO
                                {
                                    Id = Convert.ToInt64(dataRow.Field<Int64>("Id")),
                                    NameEn = dataRow.Field<string>("NameEn"),
                                    NameAr = dataRow.Field<string>("NameAr")
                                }).ToList();
                                SelectList ObjContData = new SelectList(hotels, "Id", "NameAr", 0);
                                ViewBag.BrandListMap = ObjContData;
                                //TempData["CountriesList"] = CountriesList;
                            }
                            else
                            {
                                SelectList ObjContData = new SelectList("", "Id", "NameAr", 0);
                                ViewBag.BrandListMap = ObjContData;
                            }
                            if (ds.Tables[4].Rows.Count > 0)
                            {
                               var hotels = ds.Tables[4].AsEnumerable().Select(dataRow => new ManageHotelsDTO
                                {
                                    Id = Convert.ToInt64(dataRow.Field<Int64>("Id")),
                                    NameEn = dataRow.Field<string>("NameEn"),
                                    NameAr = dataRow.Field<string>("NameAr")
                                }).ToList();
                                SelectList ObjContData = new SelectList(hotels, "Id", "NameAr", 0);
                                ViewBag.BrandList = ObjContData;
                                //TempData["CountriesList"] = CountriesList;
                            }
                            else
                            {
                                SelectList ObjContData = new SelectList("", "Id", "NameAr", 0);
                                ViewBag.BrandList = ObjContData;
                            }
                            if (TempData["htlId"] == null || TempData["AmtId"] == null)
                            {
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    CountriesList = ds.Tables[0].AsEnumerable().Select(dataRow => new ManageCountriesDTO
                                    {
                                        Id = Convert.ToInt64(dataRow.Field<Int64>("CountryId")),
                                        NameEn = dataRow.Field<string>("NameEn"),
                                        NameAr = dataRow.Field<string>("NameAr")
                                    }).ToList();
                                    TempData["CountriesList"] = CountriesList;
                                }
                                else
                                {
                                    SelectList objmodeldata = new SelectList("", 0);
                                    ViewBag.cities = objmodeldata;
                                }
                                if (ds.Tables[1].Rows.Count > 0)
                                {
                                    Cityist = ds.Tables[1].AsEnumerable().Select(dataRow => new ManageCitiesDTO
                                    {
                                        Id = Convert.ToInt64(dataRow.Field<Int64>("CityId")),
                                        CountryId = Convert.ToInt64(dataRow.Field<Int64>("CountryId")),
                                        NameEn = dataRow.Field<string>("NameEn"),
                                        NameAr = dataRow.Field<string>("NameAr")
                                    }).ToList();
                                    TempData["Cityist"] = Cityist;
                                }
                                else
                                {
                                    SelectList objmodeldata = new SelectList("", 0);
                                    ViewBag.cities = objmodeldata;
                                }
                                if (ds.Tables[2].Rows.Count > 0)
                                {
                                    BranchList = ds.Tables[2].AsEnumerable().Select(dataRow => new ManageBranchesDTO
                                    {
                                        Id = Convert.ToInt64(dataRow.Field<Int64>("Id")),
                                        CityId = Convert.ToInt64(dataRow.Field<Int64>("cityId")),
                                        HotelId = Convert.ToInt64(dataRow.Field<Int64>("HotelId")),
                                        NameEn = dataRow.Field<string>("NameEn"),
                                        NameAr = dataRow.Field<string>("NameAr")
                                    }).ToList();
                                    TempData["BranchList"] = BranchList;
                                }
                                else
                                {
                                    SelectList objmodeldata = new SelectList("", 0);
                                    ViewBag.BranchList = objmodeldata;
                                }
                            }
                            TempData["htlId"] = HtId;
                        }
                    }
                }
                if (TempData["CountriesList"] != null)
                {
                    ViewBag.countries = TempData["CountriesList"];
                }
                if (TempData["Cityist"] != null)
                {
                    ViewBag.cities = TempData["Cityist"];
                }
                if (TempData["BranchList"] != null)
                {
                    ViewBag.BranchList = TempData["BranchList"];
                }
            }


            return View();
        }
        public async Task<ActionResult> getAmenities(string AmenitiesIDs)
        {
            using (HttpClient client = new HttpClient())
            {
                CommonHeader.setHeaders(client);
                try
                {
                    bool status = false;
                    FindAHotelDTO a = new FindAHotelDTO();
                    List<FindAHotelDTO> aa = new List<FindAHotelDTO>();
                    List<FindAHotelDTO> ListofBranches = new List<FindAHotelDTO>();
                    a.Id = 6;
                    a.AmenityIds = FixedAmenitiesIds;
                    if (!string.IsNullOrEmpty(a.AmenityIds))
                    {
                        //a.AmenityIds = AmenitiesIDs == null ? "" : AmenitiesIDs.Remove(AmenitiesIDs.Length-1, 1);
                        StringBuilder sb = new StringBuilder(a.AmenityIds);
                        sb.Length--;
                        a.AmenityIds = sb.ToString();
                    }
                    a.BrandIds = FixedBrandIds;
                    if (!string.IsNullOrEmpty(a.BrandIds))
                    {
                        //a.AmenityIds = AmenitiesIDs == null ? "" : AmenitiesIDs.Remove(AmenitiesIDs.Length-1, 1);
                        StringBuilder sb = new StringBuilder(a.BrandIds);
                        sb.Length--;
                        a.BrandIds = sb.ToString();
                    }
                    HttpResponseMessage responseMessage = await client.PostAsJsonAsync("api/FindAHotelAPI/NewGetHotelsList", a);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        //var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                        //var user = JsonConvert.DeserializeObject<ManageUsersDTO>(responseData);
                        var HotelsData = responseMessage.Content.ReadAsStringAsync().Result;
                        var HotelsList = JsonConvert.DeserializeObject<List<FindAHotelDTO>>(HotelsData);
                        ListofBranches = HotelsList;


                        return Json(HotelsList, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var result = "3";
                        return Json(result, JsonRequestBehavior.AllowGet);
                    }

                    //return View("Error");
                }
                catch (Exception ex)
                {
                    ErrorLogDTO err = new ErrorLogDTO();

                    return RedirectToAction("Error");
                }
            }
        }
        [HttpGet]
        public async Task<ActionResult> GetMapByAmenity(string AmenitiesIDs, string latlongzoom, string BrandIds)
        {
            using (HttpClient client = new HttpClient())
            {
                CommonHeader.setHeaders(client);
                try
                {
                    FixedAmenitiesIds = AmenitiesIDs;
                    FixedBrandIds = BrandIds;
                    bool status = false;
                    FindAHotelDTO a = new FindAHotelDTO();
                    //List<FindAHotelDTO> aa = new List<FindAHotelDTO>();
                    //List<FindAHotelDTO> ListofBranches = new List<FindAHotelDTO>();
                    a.Id = 1;
                    if (AmenitiesIDs != null || AmenitiesIDs == "")
                    {
                        a.AmenityIds = AmenitiesIDs;
                    }
                    else
                    {
                        a.Id = 0;
                    }
                    if (!string.IsNullOrEmpty(latlongzoom))
                    {
                        var Temppositions = latlongzoom;
                        Temppositions = latlongzoom.Replace("(", "");
                        Temppositions = Temppositions.Replace(")", "");
                        var positions = latlongzoom.Split(',');
                        if (positions[0] != null && positions[0].Length > 1)
                            a.Lat = positions[0];
                        else
                            a.Lat = "0";
                        if (positions[1] != null && positions[1].Length > 1)
                            a.Lng = positions[1];

                        else
                            a.Lng = "0";
                        if (positions.Length == 3)
                        {
                            if (positions[2] != null && positions[2].Length >= 1)
                                a.Zoom = positions[2];
                            else
                                a.Zoom = "0";
                        }
                        else
                            a.Zoom = "0";
                    }
                    else
                    {
                        a.Lat = "0";
                        a.Lng = "0";
                        a.Zoom = "0";
                    }

                    //HttpResponseMessage responseMessage = await client.PostAsJsonAsync("api/FindAHotelAPI/NewGetHotelsList", a);
                    //if (responseMessage.IsSuccessStatusCode)
                    //{
                    //    var HotelsData = responseMessage.Content.ReadAsStringAsync().Result;
                    //    var HotelsList = JsonConvert.DeserializeObject<List<FindAHotelDTO>>(HotelsData);
                    //    ListofBranches = HotelsList;
                    //    //a.listFindHotel = HotelsList;



                    //    var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    //    string outputOfFoos = serializer.Serialize(HotelsList);
                    //    JsonResult data= Json(HotelsList, JsonRequestBehavior.AllowGet);

                    //    a.JsonData = data;
                    //return Json(HotelsList, JsonRequestBehavior.AllowGet);
                    return View("GetMapByAmenity", a);

                    //}
                    //else
                    //{
                    //    //var result = "3";
                    //    return View("GetMapByAmenity", a);
                    //}

                    //return View("Error");
                }
                catch (Exception ex)
                {
                    ErrorLogDTO err = new ErrorLogDTO();

                    return RedirectToAction("Error");
                }
            }
        }
        public async Task<ActionResult> getData(string HtId, string para)
        {
            using (HttpClient client = new HttpClient())
            {
                CommonHeader.setHeaders(client);
                try
                {
                    List<ManageCountriesDTO> CountriesList = new List<ManageCountriesDTO>();
                    List<ManageCitiesDTO> Cityist = new List<ManageCitiesDTO>();
                    List<ManageBranchesDTO> BranchList = new List<ManageBranchesDTO>();
                    FindAHotelNewDTO obj = new FindAHotelNewDTO();
                    if (string.IsNullOrEmpty(HtId))
                    {
                        obj.CountryId = 0;
                    }
                    else
                        obj.CountryId = Convert.ToInt64(HtId);

                    if (string.IsNullOrEmpty(para))
                    {
                        obj.Id = 0;
                    }
                    else
                        obj.Id = Convert.ToInt64(para);

                    TempData["AmtId"] = para;
                    //HtId = obj.HotelId;
                    HttpResponseMessage AmenRes = await client.PostAsJsonAsync("api/FindAHotelAPI/NewMapData", obj);
                    if (AmenRes.IsSuccessStatusCode)
                    {
                        var resData = AmenRes.Content.ReadAsStringAsync().Result;
                        var res = JsonConvert.DeserializeObject<ManageBranchesDTO>(resData);
                        var data = res.datasetXml;
                        if (data != null)
                        {
                            var document = new XmlDocument();
                            document.LoadXml(data);
                            DataSet ds = new DataSet();
                            ds.ReadXml(new XmlNodeReader(document));
                            if (ds.Tables.Count > 0)
                            {
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    CountriesList = ds.Tables[0].AsEnumerable().Select(dataRow => new ManageCountriesDTO
                                    {
                                        Id = Convert.ToInt64(dataRow.Field<Int64>("CountryId")),
                                        NameEn = dataRow.Field<string>("NameEn"),
                                        NameAr = dataRow.Field<string>("NameAr")
                                    }).ToList();
                                    TempData["CountriesList"] = CountriesList;
                                }
                                else
                                {
                                    SelectList objmodeldata = new SelectList("", 0);
                                    ViewBag.cities = objmodeldata;
                                }
                                if (ds.Tables[1].Rows.Count > 0)
                                {
                                    Cityist = ds.Tables[1].AsEnumerable().Select(dataRow => new ManageCitiesDTO
                                    {
                                        Id = Convert.ToInt64(dataRow.Field<Int64>("CityId")),
                                        CountryId = Convert.ToInt64(dataRow.Field<Int64>("CountryId")),
                                        NameEn = dataRow.Field<string>("NameEn"),
                                        NameAr = dataRow.Field<string>("NameAr")
                                    }).ToList();
                                    TempData["Cityist"] = Cityist;
                                }
                                else
                                {
                                    SelectList objmodeldata = new SelectList("", 0);
                                    ViewBag.cities = objmodeldata;
                                }
                                if (ds.Tables[2].Rows.Count > 0)
                                {
                                    BranchList = ds.Tables[2].AsEnumerable().Select(dataRow => new ManageBranchesDTO
                                    {
                                        Id = Convert.ToInt64(dataRow.Field<Int64>("Id")),
                                        CityId = Convert.ToInt64(dataRow.Field<Int64>("cityId")),
                                        HotelId = Convert.ToInt64(dataRow.Field<Int64>("HotelId")),
                                        NameEn = dataRow.Field<string>("NameEn"),
                                        NameAr = dataRow.Field<string>("NameAr")
                                    }).ToList();
                                    TempData["BranchList"] = BranchList;
                                }
                                else
                                {
                                    SelectList objmodeldata = new SelectList("", 0);
                                    ViewBag.BranchList = objmodeldata;
                                }
                            }
                            TempData["htlId"] = HtId;
                        }
                    }
                    return RedirectToAction("Index");
                    //return View();
                    //return Json(BranchList, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    ErrorLogDTO err = new ErrorLogDTO();
                    return RedirectToAction("Error");
                }
            }
        }
        [HttpGet]
        public async Task<ActionResult> GetMapData(string HtId, string para, string BIds)
        {
            using (HttpClient client = new HttpClient())
            {
                CommonHeader.setHeaders(client);
                try
                {
                    //ManageHotelAmenitiesDTO aObj = new ManageHotelAmenitiesDTO();
                    //List<ManageHotelAmenitiesDTO> a = new List<ManageHotelAmenitiesDTO>();
                    //ManageCountriesDTO CountryObj = new ManageCountriesDTO();
                    //ManageCitiesDTO cityObj = new ManageCitiesDTO();
                    //List<ManageCountriesDTO> CountryList = new List<ManageCountriesDTO>();
                    //List<ManageCitiesDTO> CityList = new List<ManageCitiesDTO>();
                    ////List<ManageBranchesDTO> BranchList = new List<ManageBranchesDTO>();
                    //HttpResponseMessage AmenitiesRes = await client.PostAsJsonAsync("api/HotelAmenitiesAPI/NewGetHotelAmenity", aObj);
                    //if (AmenitiesRes.IsSuccessStatusCode)
                    //{
                    //    var AmenitiesData = AmenitiesRes.Content.ReadAsStringAsync().Result;
                    //    var AmenitiesList = JsonConvert.DeserializeObject<List<ManageHotelAmenitiesDTO>>(AmenitiesData);
                    //    a = AmenitiesList;
                    //    List<ManageHotelAmenitiesDTO> AmnList = AmenitiesList;
                    //    SelectList objModelData = new SelectList(AmnList, "Id", "NameEn", 0);
                    //    ViewBag.HotelAmenities = objModelData;
                    //}
                    //HttpResponseMessage CountryRes = await client.PostAsJsonAsync("api/CountriesAPI/NewCountry", CountryObj);
                    //if (CountryRes.IsSuccessStatusCode)
                    //{
                    //    var CountryData = CountryRes.Content.ReadAsStringAsync().Result;
                    //    var ContryList = JsonConvert.DeserializeObject<List<ManageCountriesDTO>>(CountryData);
                    //    SelectList ObjContData = new SelectList(ContryList, "Id", "NameEn", 0);
                    //    ViewBag.CoutrList = ObjContData;
                    //}
                    List<ManageCountriesDTO> CountriesList = new List<ManageCountriesDTO>();
                    List<ManageCitiesDTO> Cityist = new List<ManageCitiesDTO>();
                    List<ManageBranchesDTO> BranchList = new List<ManageBranchesDTO>();
                    FindAHotelNewDTO obj = new FindAHotelNewDTO();
                    if (string.IsNullOrEmpty(HtId))
                    {
                        //obj.CountryId = 0;
                        obj.CityId = 0;
                    }
                    else
                        //obj.CountryId = Convert.ToInt64(HtId);
                        obj.CityId = Convert.ToInt64(HtId);


                    //if (string.IsNullOrEmpty(para))
                    //{
                    //    obj.Id = 0;
                    //}
                    //else
                    //    obj.Id = Convert.ToInt64(para);

                    if (!string.IsNullOrEmpty(para))
                        obj.AmenityIds = para.Remove(para.Length - 1);

                    if (!string.IsNullOrEmpty(BIds))
                        obj.BrandIds = BIds.Remove(BIds.Length - 1);

                    obj.HotelId = 0;
                    obj.Id = 6;

                    //TempData["AmtId"] = para;
                    //HtId = obj.HotelId;;
                    HttpResponseMessage AmenRes = await client.PostAsJsonAsync("api/FindAHotelAPI/NewMapData", obj);
                    if (AmenRes.IsSuccessStatusCode)
                    {
                        var resData = AmenRes.Content.ReadAsStringAsync().Result;
                        var res = JsonConvert.DeserializeObject<ManageBranchesDTO>(resData);
                        var data = res.datasetXml;
                        if (data != null)
                        {
                            var document = new XmlDocument();
                            document.LoadXml(data);
                            DataSet ds = new DataSet();
                            ds.ReadXml(new XmlNodeReader(document));
                            if (ds.Tables.Count > 0)
                            {
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    CountriesList = ds.Tables[0].AsEnumerable().Select(dataRow => new ManageCountriesDTO
                                    {
                                        Id = Convert.ToInt64(dataRow.Field<Int64>("CountryId")),
                                        NameEn = dataRow.Field<string>("NameEn"),
                                        //NameAr = dataRow.Field<string>("NameAr")
                                    }).ToList();
                                    ViewBag.countries = CountriesList;
                                    //ViewData["CountriesList"] = CountriesList;
                                }
                                else
                                {
                                    //SelectList objmodeldata = new SelectList("", 0);
                                    //ViewBag. = objmodeldata;
                                    ViewBag.countries = CountriesList;

                                }
                                if (ds.Tables[1].Rows.Count > 0)
                                {
                                    Cityist = ds.Tables[1].AsEnumerable().Select(dataRow => new ManageCitiesDTO
                                    {
                                        Id = Convert.ToInt64(dataRow.Field<Int64>("CityId")),
                                        CountryId = Convert.ToInt64(dataRow.Field<Int64>("CountryId")),
                                        NameEn = dataRow.Field<string>("NameEn"),
                                        NameAr = dataRow.Field<string>("NameAr")
                                    }).ToList();
                                    ViewBag.cities = Cityist;
                                    //ViewBag.cities = Cityist;
                                    //ViewData["Cityist"] = Cityist;
                                }
                                else
                                {
                                    //    SelectList objmodeldata = new SelectList("", 0);
                                    //    ViewBag.cities = objmodeldata;
                                    ViewBag.cities = null;

                                }
                                if (ds.Tables[2].Rows.Count > 0)
                                {
                                    BranchList = ds.Tables[2].AsEnumerable().Select(dataRow => new ManageBranchesDTO
                                    {
                                        Id = Convert.ToInt64(dataRow.Field<Int64>("Id")),
                                        CityId = Convert.ToInt64(dataRow.Field<Int64>("cityId")),
                                        HotelId = Convert.ToInt64(dataRow.Field<Int64>("HotelId")),
                                        NameEn = dataRow.Field<string>("NameEn"),
                                        NameAr = dataRow.Field<string>("NameAr")
                                    }).ToList();
                                    ViewBag.BranchList = BranchList;
                                    //ViewData["BranchList"] = BranchList;
                                }
                                else
                                {
                                    //    SelectList objmodeldata = new SelectList("", 0);
                                    //    ViewBag.BranchList = objmodeldata;
                                    ViewBag.BranchList = null;

                                }
                            }
                            //TempData["htlId"] = HtId;
                        }
                    }
                    FindAHotelDTO outobj = new FindAHotelDTO();
                    outobj.CityEn = "TempCity";
                    return View("GetMapData", outobj);
                    //return View();
                    //return Json(BranchList, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    ErrorLogDTO err = new ErrorLogDTO();
                    return RedirectToAction("Error");
                }
            }
        }
    }
}