﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using BoudlGroup.Handlers;
using System.Net.Mail;
using PagedList;

namespace BoudlGroup.Areas.ar.Controllers
{
    public class HomeController : Controller
    {
        // GET: ar/Home
        string img = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();
        public async Task<ActionResult> Index()
        {
            
            using (HttpClient client = new HttpClient())
            {
                CommonHeaders.setHeaders(client);

                try
                {
                    HomePageViewModel obj = new HomePageViewModel();
                    obj.Id = 6;
                    List<HomePageViewModel> hotellist = new List<HomePageViewModel>();
                    List<HomePageViewModel> branchlist = new List<HomePageViewModel>();
                    List<HomePageViewModel> BannersImages = new List<HomePageViewModel>();
                    List<HomePageViewModel> bannerlist = new List<HomePageViewModel>();
                    List<HomePageAboutSectionDTO> aboutList = new List<HomePageAboutSectionDTO>();
                    HttpResponseMessage responseMessage = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/BranchBookingService", obj);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                        var categories = JsonConvert.DeserializeObject<HomePageViewModel>(responseData);
                        var data = categories.datasetxml;

                        if (data != null)
                        {
                            var document = new XmlDocument();
                            document.LoadXml(data);
                            DataSet ds = new DataSet();
                            ds.ReadXml(new XmlNodeReader(document));
                            if (ds.Tables.Count > 0)
                            {
                                if (ds.Tables[3].Rows.Count > 0)
                                {
                                    hotellist = ds.Tables[3].AsEnumerable().Select(dataRow => new HomePageViewModel
                                    {
                                        Id = Convert.ToInt64(dataRow.Field<long>("Id")),
                                        NameEn = dataRow.Field<string>("NameEn")
                                    }).ToList();
                                    obj.HotelsList = hotellist;
                                }
                                else
                                {
                                    obj.HotelsList = null;
                                }

                                if (ds.Tables[4].Rows.Count > 0)
                                {
                                    branchlist = ds.Tables[4].AsEnumerable().Select(dataRow => new HomePageViewModel
                                    {
                                        Id = Convert.ToInt64(dataRow.Field<long>("Id")),
                                        url = dataRow.Field<string>("bookingURL"),
                                        hotelid = Convert.ToInt64(dataRow.Field<long>("HotelId")),
                                        NameEn = dataRow.Field<string>("NameEn")
                                    }).ToList();
                                    obj.BranchList = branchlist;
                                }
                                else
                                {
                                    obj.BranchList = null;
                                }

                                //Hotel Home page Aboutus Section
                                if (ds.Tables[5].Rows.Count > 0)
                                {
                                    aboutList = ds.Tables[5].AsEnumerable().Select(DataRow => new HomePageAboutSectionDTO
                                    {
                                        HotelId = DataRow.Field<long>("HotelId"),
                                        DescriptionEn = DataRow.Field<string>("DescriptionEn"),
                                        DescriptionAr = DataRow.Field<string>("DescriptionAr"),
                                        ImageEn = img + "/" + DataRow.Field<string>("ImageEn"),
                                        ImageAr = img + "/" + DataRow.Field<string>("ImageAr"),
                                    }).ToList();
                                    ViewBag.HomeAboutList = aboutList;
                                }
                                else
                                {
                                    SelectList aboutList1 = new SelectList("", 0);
                                    //ViewBag.HomeAboutList = aboutList1;
                                    ViewBag.HomeAboutList = null;
                                }

                                if (ds.Tables[6].Rows.Count > 0)
                                {
                                    
                                        BannersImages = (from row in ds.Tables[6].AsEnumerable()
                                                         where row.Field<string>("Language") != "2"
                                                         select new HomePageViewModel
                                                         {
                                                             Id = Convert.ToInt64(row.Field<long>("Id")),
                                                             url = row.Field<string>("SourcePath"),
                                                             NavigationUrl=row.Field<string>("NavigationUrl"),
                                                             mediaTypeId = row.Field<Int64>("MediaTypeId")
                                                         }
                                                    ).ToList();
                                    

                                    BannersImages = BannersImages.OrderByDescending(i => i.mediaTypeId).ToList();
                                    if (BannersImages.Count > 0)
                                    {
                                        switch (BannersImages.First().mediaTypeId)
                                        {
                                            case 1:
                                                obj.slideshowType = 1;
                                                break;
                                            case 2:
                                                obj.slideshowType = 2;
                                                break;
                                            case 3:
                                                obj.slideshowType = 3;
                                                break;
                                        }
                                    }

                                    BannersImages = BannersImages.Where(i => i.mediaTypeId == (Int64)obj.slideshowType).ToList();
                                    if (obj.slideshowType == 1 || obj.slideshowType == 2)
                                    {
                                        BannersImages = BannersImages.Select(i => new HomePageViewModel { Id = i.Id, url = img + "/" + i.url, mediaTypeId = i.mediaTypeId }).ToList();
                                    }

                                    obj.SlideshowImages = BannersImages;

                                }
                            }
                        }
                    }
                    else //if API execution is false
                    {
                        return View("Error");
                    }
                    
                    return View("Index", obj);
                    
                }
                catch (Exception ex)
                {
                    return View("Error");
                }
            }
        }
        public async Task<JsonResult> GetHotels()
        {
            using (HttpClient client = new HttpClient())
            {
                CommonHeaders.setHeaders(client);
                try
                {
                    List<HomePageViewModel> hotellist = new List<HomePageViewModel>();
                    List<HomePageViewModel> branchlist = new List<HomePageViewModel>();
                    List<HomePageViewModel> mediatypelist = new List<HomePageViewModel>();
                    List<HomePageViewModel> bannerlist = new List<HomePageViewModel>();

                    HomePageViewModel obj = new HomePageViewModel();
                    SelectList ddlsources = new SelectList("", "CityId", "City", 0);
                    HttpResponseMessage responseMessage = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/BranchBookingService", obj);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                        var categories = JsonConvert.DeserializeObject<HomePageViewModel>(responseData);
                        var data = categories.datasetxml;

                        if (data != null)
                        {
                            var document = new XmlDocument();
                            document.LoadXml(data);
                            DataSet ds = new DataSet();
                            ds.ReadXml(new XmlNodeReader(document));
                            if (ds.Tables.Count > 0)
                            {
                                if (ds.Tables[3].Rows.Count > 0)
                                {
                                    hotellist = ds.Tables[3].AsEnumerable().Select(dataRow => new HomePageViewModel
                                    {
                                        Id = Convert.ToInt64(dataRow.Field<long>("Id")),
                                        NameEn = dataRow.Field<string>("NameEn"),
                                        NameAr = dataRow.Field<string>("NameAr")
                                    }).ToList();
                                    obj.HotelsList = hotellist;
                                    ViewData["ddlhotelslist"] = new SelectList(hotellist, "Id", "NameEn", 0);
                                }
                                else
                                {
                                    obj.HotelsList = null;
                                }
                                if (ds.Tables[4].Rows.Count > 0)
                                {
                                    branchlist = ds.Tables[4].AsEnumerable().Select(dataRow => new HomePageViewModel
                                    {
                                        Id = Convert.ToInt64(dataRow.Field<long>("Id")),
                                        url = dataRow.Field<string>("bookingURL"),
                                        hotelid = Convert.ToInt64(dataRow.Field<long>("HotelId")),
                                        NameEn = dataRow.Field<string>("NameEn"),
                                        NameAr = dataRow.Field<string>("NameAr")
                                    }).ToList();
                                    obj.BranchList = branchlist;
                                    ViewData["ddlbrancheslist"] = new SelectList(hotellist, "hotelid", "NameEn", 0);
                                }
                                else
                                {
                                    obj.BranchList = null;
                                }

                            }
                        }
                    }
                    return Json(new { FirstList = hotellist, SecondList = branchlist }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    ErrorLogDTO err = new ErrorLogDTO();
                    return Json(new SelectList("", "Value", "Text"));
                }
            }
        }
        public async Task<ActionResult> ExecutiveBoard(string param)
        {
            using (HttpClient client = new HttpClient())
            {
                CommonHeaders.setHeaders(client);
                try
                {
                    HomePageViewModel obj = new HomePageViewModel();
                    List<HomePageViewModel> boardmembersList = new List<HomePageViewModel>();

                    HttpResponseMessage responseMessage = await client.PostAsJsonAsync("api/BoardMembersAPI/NewGetBoardMembersList", obj);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                        var categories = JsonConvert.DeserializeObject<HomePageViewModel>(responseData);
                        var data = categories.datasetxml;

                        if (data != null)
                        {
                            var document = new XmlDocument();
                            document.LoadXml(data);
                            DataSet ds = new DataSet();
                            ds.ReadXml(new XmlNodeReader(document));
                            if (ds.Tables.Count > 0)
                            {

                                if (ds.Tables[1].Rows.Count > 0)
                                {
                                    boardmembersList = ds.Tables[1].AsEnumerable().Select(dataRow => new HomePageViewModel
                                    {
                                        Id = dataRow.Field<long>("Id"),
                                        NameEn = dataRow.Field<string>("NameEn"),
                                        NameAr = dataRow.Field<string>("NameAr"),
                                        DesignationEn = dataRow.Field<string>("DesignationEn"),
                                        DesignationAr = dataRow.Field<string>("DesignationAr"),
                                        MsgTitleEn = dataRow.Field<string>("MsgTitleEn"),
                                        MsgTitleAr = dataRow.Field<string>("MsgTitleAr"),
                                        MessageEn = dataRow.Field<string>("MessageEn"),
                                        MessageAr = dataRow.Field<string>("MessageAr"),
                                        IsBoardMember = Convert.ToBoolean(dataRow.Field<bool>("IsBoardMember")),
                                        HasMessage = Convert.ToBoolean(dataRow.Field<bool>("HasMessage")),
                                        ImagePath = img + "/" + dataRow.Field<string>("ImagePath"),
                                        CreatedOn = dataRow.Field<DateTime>("CreatedOn")
                                    }).ToList();
                                    obj.boardmemberslist = boardmembersList;
                                }
                                else
                                {
                                    obj.boardmemberslist = null;
                                }
                            }
                        }
                    }
                    else
                    {
                        return View("Error");

                    }
                    return View("ExecutiveBoardView", obj);
                }
                catch (Exception ex)
                {
                    return View("Error");

                }
            }
        }

        public async Task<ActionResult> ChairmanMessage()
        {
            using (HttpClient client = new HttpClient())
            {
                CommonHeaders.setHeaders(client);
                try
                {
                    HomePageViewModel obj = new HomePageViewModel();
                    List<HomePageViewModel> boardmembersList = new List<HomePageViewModel>();

                    HttpResponseMessage responseMessage = await client.PostAsJsonAsync("api/BoardMembersAPI/NewGetBoardMembersList", obj);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                        var categories = JsonConvert.DeserializeObject<HomePageViewModel>(responseData);
                        var data = categories.datasetxml;
                        #region APIData
                        if (data != null)
                        {
                            var document = new XmlDocument();
                            document.LoadXml(data);
                            DataSet ds = new DataSet();
                            ds.ReadXml(new XmlNodeReader(document));
                            if (ds.Tables.Count > 0)
                            {

                                if (ds.Tables[1].Rows.Count > 0)
                                {
                                    boardmembersList = ds.Tables[1].AsEnumerable().Select(dataRow => new HomePageViewModel
                                    {
                                        Id = dataRow.Field<long>("Id"),
                                        NameEn = dataRow.Field<string>("NameEn"),
                                        NameAr = dataRow.Field<string>("NameAr"),
                                        DesignationEn = dataRow.Field<string>("DesignationEn"),
                                        DesignationAr = dataRow.Field<string>("DesignationAr"),
                                        MsgTitleEn = dataRow.Field<string>("MsgTitleEn"),
                                        MsgTitleAr = dataRow.Field<string>("MsgTitleAr"),
                                        MessageEn = dataRow.Field<string>("MessageEn"),
                                        MessageAr = dataRow.Field<string>("MessageAr"),
                                        IsBoardMember = Convert.ToBoolean(dataRow.Field<bool>("IsBoardMember")),
                                        HasMessage = Convert.ToBoolean(dataRow.Field<bool>("HasMessage")),
                                        ImagePath = img + "/" + dataRow.Field<string>("ImagePath"),
                                        CreatedOn = dataRow.Field<DateTime>("CreatedOn")
                                    }).ToList();
                                    obj.boardmemberslist = boardmembersList;
                                }
                                else
                                {
                                    obj.boardmemberslist = null;
                                }
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        return View("Error");
                    }

                    
                    return View("ChairmanMessageAndExecutiveBoard", obj);
                
                }
                catch (Exception ex)
                {
                    return View("Error");

                }
            }
        }

        public ActionResult ContactUs()
        {

            HomePageViewModel model = new HomePageViewModel();
            model.message = "";
            ViewData["MailMsg"] = "0";

            model.message = "3";
            return View(model);
            
        }
        [HttpPost]
        public ActionResult ContactUs(HomePageViewModel model, string send)
        {
            string mailTo = "shujat@creative-sols.com ";
            string mailfrom = model.Email;
            //"info@dr-cafe.com";
            string mailCC = "m.baig@creative-sols.com";
            //"WEBMASTER@dr-cafe.com";
            string Subject = "Contact form Form";
            string strHTML = "<strong>" + model.NameEn + "</strong> has sent the following information:<br/>" + model.Comment + "";
            
            string msg = sendEmail(mailfrom, mailTo, strHTML, Subject, mailCC);

            if (msg == "Success")
            {
                ViewData["MailMsg"] = "1";
                model.message = "1";
                
                return View("ContactUs");
            }
            else
            {
                HomePageViewModel model2 = new HomePageViewModel();
                ViewData["MailMsg"] = "2";
                model.message = "2";
                //return View("ContactUs");
                return View("ContactUs");

            }
        }
        private string sendEmail(string mailfrom, string mailTo, string strHTML, string Subject, string mailCC)
        {
            try
            {
                string msg1 = string.Empty;
                MailMessage mMailMessage = new MailMessage();
                mMailMessage.From = new MailAddress(mailfrom);
                mMailMessage.To.Add(new MailAddress(mailTo));

                mMailMessage.Subject = Subject;
                mMailMessage.Body = strHTML;
                mMailMessage.IsBodyHtml = true;
                mMailMessage.Priority = MailPriority.Normal;
                SmtpClient mSmtpClient = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["SMTPServer"],
                Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["smtpPort"]));
                mSmtpClient.UseDefaultCredentials = false;
                mSmtpClient.Credentials = new System.Net.NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["mailFrom"].ToString(),
                System.Configuration.ConfigurationManager.AppSettings["mailCredentialPassword"].ToString());
                mSmtpClient.Send(mMailMessage);
                msg1 = "Success";
                return msg1;
            }
            catch (Exception ex)
            {
                return ex.Message.ToString(); ;
            }
        }
        public ActionResult MissionVision()
        {
            
                return View();
            
        }
        public ActionResult History()
        {
            
                return View();
            
        }

        public async Task<ActionResult> News(string name)
        {
            using (HttpClient client = new HttpClient())
            {
                CommonHeaders.setHeaders(client);

                try
                {
                    HomePageViewModel obj = new HomePageViewModel();
                    if (name == null)
                        obj.Slug = "";
                    else
                        obj.Slug = name;
                    List<HomePageViewModel> hotellist = new List<HomePageViewModel>();
                    List<HomePageViewModel> branchlist = new List<HomePageViewModel>();
                    List<HomePageViewModel> BannersImages = new List<HomePageViewModel>();
                    List<HomePageViewModel> bannerlist = new List<HomePageViewModel>();
                    HttpResponseMessage responseMessage = await client.PostAsJsonAsync("api/NewsAPI/NewGetHotelsList", obj);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                        var categories = JsonConvert.DeserializeObject<HomePageViewModel>(responseData);
                        var data = categories.datasetxml;

                        if (data != null)
                        {
                            var document = new XmlDocument();
                            document.LoadXml(data);
                            DataSet ds = new DataSet();
                            ds.ReadXml(new XmlNodeReader(document));
                            if (ds.Tables.Count > 0)
                            {

                                if (ds.Tables[2].Rows.Count > 0)
                                {
                                    hotellist = ds.Tables[2].AsEnumerable().Select(dataRow => new HomePageViewModel
                                    {
                                        Id = Convert.ToInt64(dataRow.Field<long>("Id")),
                                        NameEn = dataRow.Field<string>("NameEn"),
                                        NameAr = dataRow.Field<string>("NameAr"),
                                        Slug = dataRow.Field<string>("Slug"),
                                        ImagePath = img + "/" + dataRow.Field<string>("ImagePath"),
                                        CreatedOn = dataRow.Field<DateTime>("CreatedOn"),
                                        DescEn = dataRow.Field<string>("DescEn"),
                                        DescAr = dataRow.Field<string>("DescAr")
                                    }).ToList();
                                    obj.NewsList = hotellist;
                                    TempData["News"] = hotellist;
                                }
                                else
                                {
                                    obj.NewsList = null;
                                    TempData["News"] = hotellist;
                                }
                            }
                        }
                    }

                    else //if API execution is false
                    {
                        return View("Error");
                    }
                    if (TempData["obj"] != null)
                    {
                    }
                    
                    //If any specific article is being read, load particular news view
                    if (name != null)
                    {   
                        return View("ParticularNewsArticleView", obj);
                    }

                    //Load following view only when news listing is being rendered
                    return View(obj);
                }
                catch (Exception ex)
                {
                    return View("Error");
                }
            }
        }
        public async Task<ActionResult> NewPage(int page = 1, int pagsize = 6)
        {
            List<HomePageViewModel> NewsList = new List<HomePageViewModel>();

            NewsList = (List<HomePageViewModel>)TempData["News"];
            PagedList<HomePageViewModel> model = new PagedList<HomePageViewModel>(NewsList, page, pagsize);
            return PartialView("NewPage", model);
        }
        public ActionResult ChangeLanguage()
        {
            String newURL = "/";
            if (Request.UrlReferrer != null)
            {
                String hostFolder = System.Configuration.ConfigurationManager.AppSettings["hostFolder"].ToString().ToLower();
                String applPath = Request.Url.AbsolutePath.ToLower();

                String refPath = Request.UrlReferrer.ToString().ToLower();
                String basePath = (Request.Url.ToString().ToLower().Replace(applPath, ""));
                String userPath = refPath.Replace(hostFolder, "");

                bool arabicUser = refPath.StartsWith(hostFolder + "ar/", true, System.Globalization.CultureInfo.InvariantCulture);

                newURL = hostFolder + (arabicUser == true ? userPath.Substring(3) : "ar/" + userPath);
                return Redirect(newURL);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
    }
}