﻿using System.Web.Mvc;

namespace BoudlGroup.Areas.ar
{
    public class arAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "ar";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            

            context.MapRoute(
              name: "NewsGroupAr",
              url: "ar/News/{name}",
              defaults: new { Areas = "ar", Controller = "Home", Action = "News" },
              namespaces: new[] { "BoudlGroup.Areas.ar.Controllers" }
          );
            context.MapRoute(
                "ar_default",
                "ar/{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                namespaces: new[] { "BoudlGroup.Areas.ar.Controllers" }
            );

        }
    }
}