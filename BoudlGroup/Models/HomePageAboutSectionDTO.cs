﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace BoudlGroup
{
    public class HomePageAboutSectionDTO
    {
        public long Id { get; set; }
        public long HotelId { get; set; }
        [Display(Name = "Description (English)")]
        [Required(ErrorMessage = "*Please Enter Name (English)", AllowEmptyStrings = false)]
        // [StringLength(50, MinimumLength = 1, ErrorMessage = "*Max Length is 50")]
        public string DescriptionEn { get; set; }
        [Display(Name = "Description (Arabic)")]
        [Required(ErrorMessage = "*Please Enter Name (Arabic)", AllowEmptyStrings = false)]
        public string DescriptionAr { get; set; }
        public string ImageEn { get; set; }
        public string ImageAr { get; set; }

        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public long CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public long ModifiedBy { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime DeletedOn { get; set; }
        public long DeletedBy { get; set; }
        public IEnumerable<HomePageAboutSectionDTO> homepageaboutsectionlist { get; set; }
        public string datasetxml { get; set; }
        public string ContentType { get; set; }
        public string filepath { get; set; }
        public string hotelname { get; set; }
        public string Hotel { get; set; }
        public int FlagId { get; set; }
        public string message { get; set; }

    }
}