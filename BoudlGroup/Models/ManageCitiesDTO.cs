﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Boudl.Client.Mvc
{
    public class ManageCitiesDTO
    {
        public long Id { get; set; }
        [Display(Name ="Name (English)")]
        [Required(ErrorMessage = "*Please Enter Name (English)", AllowEmptyStrings = false)]
        [RegularExpression(@"^[a-zA-Z0-9\s]+$", ErrorMessage = "Special characters are not allowed.")]
        [StringLength(50, MinimumLength = 1, ErrorMessage = "*Max Length is 50")]
        public string NameEn { get; set; }
        [Display(Name ="Name (Arabic)")]
        [Required(ErrorMessage = "*Please Enter Name (Arabic)", AllowEmptyStrings = false)]
        [RegularExpression(@"^[\u0621-\u064A\u0660-\u0669 a-zA-Z0-9\s]+$", ErrorMessage = "Special characters are not allowed.")]
        [StringLength(50, MinimumLength = 1, ErrorMessage = "*Max Length is 50")]
        public string NameAr { get; set; }
        public Int64 HotelId { get; set; }
        [Display(Name = "Country")]
        [Required(ErrorMessage = "*{0} is required.")]
        public long CountryId { get; set; }
        public bool isSelected { get; set; }
        public bool isActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public int CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public DateTime DeletedOn { get; set; }
        public int ModifiedBy { get; set; }
        public int DeletedBy { get; set; }
        public bool isDeleted { get; set; }

        public IEnumerable<ManageCitiesDTO> Citieslist { get; set; }
        [Display(Name ="Country")]
        //[Required(ErrorMessage = "*Please Select Country", AllowEmptyStrings = false)]
        [Required(ErrorMessage = "{0} is required.")]
        public IEnumerable<ManageCitiesDTO> Countrieslist { get; set; }
        public IEnumerable<ManageCitiesDTO> CountrieslistforDropDown { get; set; }

        public string countryName { get; set; }
        public string message { get; set; }
        public int FlagId { get; set; }
        public string datasetxml { get; set; }

        public string CityName { get; set; }
        public string CityNameAr { get; set; }

    }
}