﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Boudl.Client.Mvc
{
    public class ManageHotelAmenitiesDTO
    {
        public long Id { get; set; }
        [Display(Name = "Name (English)")]
        [RegularExpression(@"^[a-zA-Z0-9\s]+$", ErrorMessage = "Special characters are not allowed.")]
        [Required(ErrorMessage = "*Please Enter Name (English)", AllowEmptyStrings = false)]
        public string NameEn { get; set; }
        [Display(Name = "Name (Arabic)")]
        [RegularExpression(@"^[\u0621-\u064A\u0660-\u0669 a-zA-Z0-9\s]+$", ErrorMessage = "Special characters are not allowed.")]
        [Required(ErrorMessage = "*Please Enter Name (Arabic)", AllowEmptyStrings = false)]
        public string NameAr { get; set; }
        [Display(Name = "Icon Class")]
        [Required(ErrorMessage = "*Please enter Icon class", AllowEmptyStrings = false)]
        public string IconClass { get; set; }
        [Display(Name = "Icon")]
        public string IconImage { get; set; }

        public bool isSelected { get; set; }
        public bool isActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public long CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public DateTime DeletedOn { get; set; }
        public long ModifiedBy { get; set; }
        public long DeletedBy { get; set; }
        public bool isDeleted { get; set; }

        public string message { get; set; }
        public int FlagId { get; set; }
        public string datasetxml { get; set; }
        public List<ManageHotelAmenitiesDTO> ManageHotelAmenitiesList { get; set; }
        public string imagepath { get; set; }
        public string ContentType { get; set; }
        public string filepath { get; set; }

       

    }
}