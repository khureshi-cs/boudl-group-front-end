﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using BoudlGroup.Handlers;
using System.Net.Mail;

namespace BoudlGroup.Controllers
{
    public class HomeController : BaseController
    {
        string img = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();
        public async Task<ActionResult> Index()
        {
            string lang = VerifyUserSession();
            using (HttpClient client = new HttpClient())
            {
                CommonHeaders.setHeaders(client);

                try
                {
                    HomePageViewModel obj = new HomePageViewModel();
                    obj.Id = 6;
                    List<HomePageViewModel> hotellist = new List<HomePageViewModel>();
                    List<HomePageViewModel> branchlist = new List<HomePageViewModel>();
                    List<HomePageViewModel> BannersImages = new List<HomePageViewModel>();
                    List<HomePageViewModel> bannerlist = new List<HomePageViewModel>();
                    HttpResponseMessage responseMessage = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/BranchBookingService", obj);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                        var categories = JsonConvert.DeserializeObject<HomePageViewModel>(responseData);
                        var data = categories.datasetxml;

                        if (data != null)
                        {
                            var document = new XmlDocument();
                            document.LoadXml(data);
                            DataSet ds = new DataSet();
                            ds.ReadXml(new XmlNodeReader(document));
                            if (ds.Tables.Count > 0)
                            {
                                if (ds.Tables[3].Rows.Count > 0)
                                {
                                    hotellist = ds.Tables[3].AsEnumerable().Select(dataRow => new HomePageViewModel
                                    {
                                        Id = Convert.ToInt64(dataRow.Field<long>("Id")),
                                        NameEn = dataRow.Field<string>("NameEn")
                                    }).ToList();
                                    obj.HotelsList = hotellist;
                                }
                                else
                                {
                                    obj.HotelsList = null;
                                }

                                if (ds.Tables[4].Rows.Count > 0)
                                {
                                    branchlist = ds.Tables[4].AsEnumerable().Select(dataRow => new HomePageViewModel
                                    {
                                        Id = Convert.ToInt64(dataRow.Field<long>("Id")),
                                        url = dataRow.Field<string>("bookingURL"),
                                        hotelid = Convert.ToInt64(dataRow.Field<long>("HotelId")),
                                        NameEn = dataRow.Field<string>("NameEn")
                                    }).ToList();
                                    obj.BranchList = branchlist;
                                }
                                else
                                {
                                    obj.BranchList = null;
                                }
                                if (ds.Tables[6].Rows.Count > 0)
                                {
                                    if(lang=="En")
                                    {
                                        BannersImages = (from row in ds.Tables[6].AsEnumerable()
                                                    where row.Field<string>("Language")=="2"
                                                    select new HomePageViewModel {
                                                        Id = Convert.ToInt64(row.Field<long>("Id")),
                                                        url =  row.Field<string>("SourcePath"),
                                                        mediaTypeId = row.Field<Int64>("MediaTypeId")
                                                    } 
                                                    ).ToList();
                                        
                                    }
                                    else
                                    {
                                        BannersImages = (from row in ds.Tables[6].AsEnumerable()
                                                         where row.Field<string>("Language") == "1"
                                                         select new HomePageViewModel
                                                         {
                                                             Id = Convert.ToInt64(row.Field<long>("Id")),
                                                             url = row.Field<string>("SourcePath"),
                                                             mediaTypeId = row.Field<Int64>("MediaTypeId")
                                                         }
                                                    ).ToList();
                                    }

                                    BannersImages = BannersImages.OrderByDescending(i => i.mediaTypeId).ToList();
                                    if(BannersImages.Count>0)
                                    {
                                        switch(BannersImages.First().mediaTypeId)
                                        {
                                            case 1:
                                                obj.slideshowType = 1;
                                                break;
                                            case 2:
                                                obj.slideshowType = 2;
                                                break;
                                            case 3:
                                                obj.slideshowType = 3;
                                                break;
                                        }
                                    }

                                    BannersImages = BannersImages.Where(i => i.mediaTypeId == (Int64)obj.slideshowType).ToList();
                                    if(obj.slideshowType == 1 || obj.slideshowType ==2)
                                    {
                                        BannersImages = BannersImages.Select(i => new HomePageViewModel { Id = i.Id, url = img + "/" + i.url, mediaTypeId = i.mediaTypeId }).ToList();
                                    }

                                    obj.SlideshowImages = BannersImages;
                                    
                                }
                                else
                                {
                                    // obj.BranchList = null;
                                }

                            }
                        }
                    }

                    else //if API execution is false
                    {
                        return View("Error");
                    }
                    
                    //return View(obj);
                    
                    if(lang == "En")
                    {
                        return View(obj);
                    }
                    else
                    {
                        return View("Index_Ar", obj);
                    }
                }
                catch (Exception ex)
                {
                    return View("Error");
                }
            }
        }
        public async Task<JsonResult> GetHotels()
        {
            using (HttpClient client = new HttpClient())
            {
                CommonHeaders.setHeaders(client);
                try
                {
                    List<HomePageViewModel> hotellist = new List<HomePageViewModel>();
                    List<HomePageViewModel> branchlist = new List<HomePageViewModel>();
                    List<HomePageViewModel> mediatypelist = new List<HomePageViewModel>();
                    List<HomePageViewModel> bannerlist = new List<HomePageViewModel>();

                    HomePageViewModel obj = new HomePageViewModel();
                    SelectList ddlsources = new SelectList("", "CityId", "City", 0);
                    HttpResponseMessage responseMessage = await client.PostAsJsonAsync("api/UserDeviceTokenAPI/BranchBookingService", obj);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                        var categories = JsonConvert.DeserializeObject<HomePageViewModel>(responseData);
                        var data = categories.datasetxml;

                        if (data != null)
                        {
                            var document = new XmlDocument();
                            document.LoadXml(data);
                            DataSet ds = new DataSet();
                            ds.ReadXml(new XmlNodeReader(document));
                            if (ds.Tables.Count > 0)
                            {
                                if (ds.Tables[3].Rows.Count > 0)
                                {
                                    hotellist = ds.Tables[3].AsEnumerable().Select(dataRow => new HomePageViewModel
                                    {
                                        Id = Convert.ToInt64(dataRow.Field<long>("Id")),
                                        NameEn = dataRow.Field<string>("NameEn"),
                                        NameAr = dataRow.Field<string>("NameAr")
                                    }).ToList();
                                    obj.HotelsList = hotellist;
                                    ViewData["ddlhotelslist"] = new SelectList(hotellist, "Id", "NameEn", 0);
                                }
                                else
                                {
                                    obj.HotelsList = null;
                                }
                                if (ds.Tables[4].Rows.Count > 0)
                                {
                                    branchlist = ds.Tables[4].AsEnumerable().Select(dataRow => new HomePageViewModel
                                    {
                                        Id = Convert.ToInt64(dataRow.Field<long>("Id")),
                                        url = dataRow.Field<string>("bookingURL"),
                                        hotelid = Convert.ToInt64(dataRow.Field<long>("HotelId")),
                                        NameEn = dataRow.Field<string>("NameEn"),
                                        NameAr = dataRow.Field<string>("NameAr")
                                    }).ToList();
                                    obj.BranchList = branchlist;
                                    ViewData["ddlbrancheslist"] = new SelectList(hotellist, "hotelid", "NameEn", 0);
                                }
                                else
                                {
                                    obj.BranchList = null;
                                }

                            }
                        }
                    }
                    return Json(new { FirstList = hotellist, SecondList = branchlist }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    ErrorLogDTO err = new ErrorLogDTO();
                    return Json(new SelectList("", "Value", "Text"));
                }
            }
        }
        public async Task<ActionResult> ExecutiveBoard(string param)
        {
            using (HttpClient client = new HttpClient())
            {
                CommonHeaders.setHeaders(client);
                try
                {
                    HomePageViewModel obj = new HomePageViewModel();
                    List<HomePageViewModel> boardmembersList = new List<HomePageViewModel>();

                    HttpResponseMessage responseMessage = await client.PostAsJsonAsync("api/BoardMembersAPI/NewGetBoardMembersList", obj);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                        var categories = JsonConvert.DeserializeObject<HomePageViewModel>(responseData);
                        var data = categories.datasetxml;

                        if (data != null)
                        {
                            var document = new XmlDocument();
                            document.LoadXml(data);
                            DataSet ds = new DataSet();
                            ds.ReadXml(new XmlNodeReader(document));
                            if (ds.Tables.Count > 0)
                            {

                                if (ds.Tables[1].Rows.Count > 0)
                                {
                                    boardmembersList = ds.Tables[1].AsEnumerable().Select(dataRow => new HomePageViewModel
                                    {
                                        Id = dataRow.Field<long>("Id"),
                                        NameEn = dataRow.Field<string>("NameEn"),
                                        NameAr = dataRow.Field<string>("NameAr"),
                                        DesignationEn = dataRow.Field<string>("DesignationEn"),
                                        DesignationAr = dataRow.Field<string>("DesignationAr"),
                                        MsgTitleEn = dataRow.Field<string>("MsgTitleEn"),
                                        MsgTitleAr = dataRow.Field<string>("MsgTitleAr"),
                                        MessageEn = dataRow.Field<string>("MessageEn"),
                                        MessageAr = dataRow.Field<string>("MessageAr"),
                                        IsBoardMember = Convert.ToBoolean(dataRow.Field<bool>("IsBoardMember")),
                                        HasMessage = Convert.ToBoolean(dataRow.Field<bool>("HasMessage")),
                                        ImagePath = img + "/" + dataRow.Field<string>("ImagePath"),
                                        CreatedOn = dataRow.Field<DateTime>("CreatedOn")
                                    }).ToList();
                                    obj.boardmemberslist = boardmembersList;
                                }
                                else
                                {
                                    obj.boardmemberslist = null;
                                }
                            }
                        }
                    }
                    else
                    {
                        return View("Error");

                    }

                    //if (param == "executiveboard")
                    string lang = VerifyUserSession();

                    if (lang == "En")
                    {
                        return View("ExecutiveBoardView", obj);
                    }
                    else
                    {
                        return View("ExecutiveBoardView_Ar", obj);
                    }

                    
                    
                }
                catch (Exception ex)
                {
                    return View("Error");

                }
            }
        }

        public async Task<ActionResult> ChairmanMessage()
        {
            using (HttpClient client = new HttpClient())
            {
                CommonHeaders.setHeaders(client);
                try
                {
                    HomePageViewModel obj = new HomePageViewModel();
                    List<HomePageViewModel> boardmembersList = new List<HomePageViewModel>();

                    HttpResponseMessage responseMessage = await client.PostAsJsonAsync("api/BoardMembersAPI/NewGetBoardMembersList", obj);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                        var categories = JsonConvert.DeserializeObject<HomePageViewModel>(responseData);
                        var data = categories.datasetxml;
                        #region APIData
                        if (data != null)
                        {
                            var document = new XmlDocument();
                            document.LoadXml(data);
                            DataSet ds = new DataSet();
                            ds.ReadXml(new XmlNodeReader(document));
                            if (ds.Tables.Count > 0)
                            {

                                if (ds.Tables[1].Rows.Count > 0)
                                {
                                    boardmembersList = ds.Tables[1].AsEnumerable().Select(dataRow => new HomePageViewModel
                                    {
                                        Id = dataRow.Field<long>("Id"),
                                        NameEn = dataRow.Field<string>("NameEn"),
                                        NameAr = dataRow.Field<string>("NameAr"),
                                        DesignationEn = dataRow.Field<string>("DesignationEn"),
                                        DesignationAr = dataRow.Field<string>("DesignationAr"),
                                        MsgTitleEn = dataRow.Field<string>("MsgTitleEn"),
                                        MsgTitleAr = dataRow.Field<string>("MsgTitleAr"),
                                        MessageEn = dataRow.Field<string>("MessageEn"),
                                        MessageAr = dataRow.Field<string>("MessageAr"),
                                        IsBoardMember = Convert.ToBoolean(dataRow.Field<bool>("IsBoardMember")),
                                        HasMessage = Convert.ToBoolean(dataRow.Field<bool>("HasMessage")),
                                        ImagePath = img + "/" + dataRow.Field<string>("ImagePath"),
                                        CreatedOn = dataRow.Field<DateTime>("CreatedOn")
                                    }).ToList();
                                    obj.boardmemberslist = boardmembersList;
                                }
                                else
                                {
                                    obj.boardmemberslist = null;
                                }
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        return View("Error");
                    }

                    string lang = VerifyUserSession();

                    if (lang == "En")
                    {
                        return View("ChairmanMessageAndExecutiveBoard", obj);
                    }
                    else
                    {
                        return View("ChairmanMessage_Ar", obj);
                    }

                    //return View("ChairmanMessageAndExecutiveBoard",obj);

                }
                catch (Exception ex)
                {
                    return View("Error");

                }
            }
        }

        public ActionResult ContactUs()
        {
            
            HomePageViewModel model = new HomePageViewModel();
            model.message = "";
            ViewData["MailMsg"] = "0";
            
            model.message = "3";

            string lang = VerifyUserSession();

            if (lang == "En")
            {
                return View(model);
            }
            else
            {
                return View("Contactus_Ar", model);
            }


            
        }
        [HttpPost]
        public ActionResult ContactUs(HomePageViewModel model,string send)
        {
            string mailTo = "shahbaz@creative-sols.com";
            string mailfrom = model.Email;
            //"info@dr-cafe.com";
            string mailCC = "m.baig@creative-sols.com";
            //"WEBMASTER@dr-cafe.com";
            string Subject = "Cntact form Form";
            string strHTML = "<strong>" + model.NameEn + "</strong> has sent the following information:<br/>" + model.Comment + "";
            
            //            "<html><body><div align='center' style='width:570px;font-family: Verdana, Geneva, sans-serif;direction:rtl;'>" +
            //"<table  style='width:570px;border:1px solid #b68f3f'><tr><td><table style='margin-right:0px;'><tr><td>" +
            //"<a href='http://www.dr-cafe.com.sa'  target='_blank'>" +
            //"<img src='http://www.dr-cafe.com/logo_arabic.jpg' alt='dr.Cafe Coffee' width='474px' height='88px'  style='border:0px'/></a></td>" +
            //"<td style='width:30px;'></td><td><table><tr><td style='width:30px'><a href='http://www.facebook.com/drcafeksa' target='_blank'>" +
            //"<img src='http://www.dr-cafe.com/DCWebsite/images/fb.jpg' alt='dr.Cafe Coffee Facebook' style='border:0px'/></a></td>" +
            //"<td style='width:30px'><a href='http://twitter.com/#/drcafeksa'  target='_blank'>" +
            //"<img src='http://www.dr-cafe.com/DCWebsite/images/tweet.jpg' alt='dr.Cafe Coffee Twitter' style='border:0px'/></a></td>" +
            //"</tr></table></td></tr></table></td></tr><tr><td><hr style='color:#b68f3f'/></td></tr><tr><td>" +
            //"<table border='0px' cellpadding='0px' cellspacing='0px' style='width:570px;'>" +
            //"<tr style='height:10px;'><td></td></tr><tr><td align='right'>تحيات د.كيف</td></tr><tr><td style='height:8px;'></td></tr><tr><td align='right'>" +
            //"<p align='justify'><font face='Verdana' style='font-size: 11px'>أشكركم على نشر التطبيق وظيفتك. وسوف نستعرض وبعد استعراض لوحة لدينا، وسوف نحصل على اتصال معكم.</font></p></td></tr> <tr><td style='height:8px;'></td>" +
            // "</tr><tr><td align='right'><p><font face='Verdana' style='font-size: 11px'> في غضون ذلك، لمعرفة المزيد عن علامتنا التجارية، فلسفتنا، رؤيتنا و البعثة، يرجى زيارة موقعنا على الانترنت" +
            // "<a href='http://www.dr-cafe.com.sa' style='color:#b8861a; font-weight:bold'>www.dr-cafe.com.sa </a>&nbsp;</font> </p></td></tr> <tr>" +
            // "<td style='height:8px;'> </td> </tr><tr><td align='right'><p>" +
            // "<font face='Verdana' style='font-size: 11px'>ونحن نغتنم هذه الفرصة في هذه المرحلة من الزمن، أن أتمنى لكم حظا سعيدا. </font>" +
            // "</p></td></tr><tr><td style='height:8px;'></td></tr><tr><td align='right'><p><font face='Verdana' style='font-size: 11px'>لأن هذا هو الرد الآلي، يرجى عدم الرد على هذه الرسالة. </font>" +
            // "</p></td></tr> <tr><td style='height:8px;'></td> </tr> <tr><td align='right'><b> د.كيف </b></td></tr><tr style='height:10px;'><td></td></tr> </table>" +
            // "</td></tr></table><br/></div> </body></html>";
            //"Thank You for your Feedback. We will get back to you shortly.";

            string msg = sendEmail(mailfrom, mailTo, strHTML, Subject, mailCC);

            string lang = VerifyUserSession();
            if (msg == "Success")
            {
                ViewData["MailMsg"] = "1";
                model.message = "1";
                //return View("ContactUs");
                if (lang == "En")
                {
                    return View("ContactUs");
                }
                else
                {
                    return View("Contactus_Ar");
                }
            }
            else
            {
                HomePageViewModel model2 = new HomePageViewModel();
                ViewData["MailMsg"] = "2";
                model.message = "2";
                //return View("ContactUs");
                if (lang == "En")
                {
                    return View("ContactUs");
                }
                else
                {
                    return View("Contactus_Ar");
                }
            }

            

            




        }
        private string sendEmail(string mailfrom, string mailTo, string strHTML, string Subject, string mailCC)
        {
            try
            {
                string msg1 = string.Empty;
                MailMessage mMailMessage = new MailMessage();
                mMailMessage.From = new MailAddress(mailfrom);
                mMailMessage.To.Add(new MailAddress(mailTo));

                mMailMessage.Subject = Subject;
                mMailMessage.Body = strHTML;
                mMailMessage.IsBodyHtml = true;
                mMailMessage.Priority = MailPriority.Normal;
                SmtpClient mSmtpClient = new SmtpClient(System.Configuration.ConfigurationManager.AppSettings["SMTPServer"],
                Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PORTNumber"]));
                mSmtpClient.UseDefaultCredentials = false;
                mSmtpClient.Credentials = new System.Net.NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["Submit_Mail"].ToString(),
                System.Configuration.ConfigurationManager.AppSettings["SMTP_PWD"].ToString());
                mSmtpClient.Send(mMailMessage);
                msg1 = "Success";
                return msg1;
            }
            catch (Exception ex)
            {
                return ex.Message.ToString(); ;
            }
        }
        public ActionResult MissionVision()
        {
            string lang = VerifyUserSession();

            if (lang == "En")
            {
                return View();
            }
            else
            {
                return View("Mission_Ar");
            }
        }
        public ActionResult History()
        {
            string lang = VerifyUserSession();
            
            if (lang == "En")
            {
                return View();
            }
            else
            {
                return View("History_Ar");
            }
        }
        
        public async Task<ActionResult> News(string name)
        {
            using (HttpClient client = new HttpClient())
            {
                CommonHeaders.setHeaders(client);

                try
                {
                    HomePageViewModel obj = new HomePageViewModel();
                    if (name == null)
                        obj.Slug = "";
                    else
                        obj.Slug = name;
                    List<HomePageViewModel> hotellist = new List<HomePageViewModel>();
                    List<HomePageViewModel> branchlist = new List<HomePageViewModel>();
                    List<HomePageViewModel> BannersImages = new List<HomePageViewModel>();
                    List<HomePageViewModel> bannerlist = new List<HomePageViewModel>();
                    HttpResponseMessage responseMessage = await client.PostAsJsonAsync("api/NewsAPI/NewGetHotelsList", obj);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                        var categories = JsonConvert.DeserializeObject<HomePageViewModel>(responseData);
                        var data = categories.datasetxml;

                        if (data != null)
                        {
                            var document = new XmlDocument();
                            document.LoadXml(data);
                            DataSet ds = new DataSet();
                            ds.ReadXml(new XmlNodeReader(document));
                            if (ds.Tables.Count > 0)
                            {

                                if (ds.Tables[2].Rows.Count > 0)
                                {
                                    hotellist = ds.Tables[2].AsEnumerable().Select(dataRow => new HomePageViewModel
                                    {
                                        Id = Convert.ToInt64(dataRow.Field<long>("Id")),
                                        NameEn = dataRow.Field<string>("NameEn"),
                                        NameAr = dataRow.Field<string>("NameAr"),
                                        Slug = dataRow.Field<string>("Slug"),
                                        ImagePath = img + "/" + dataRow.Field<string>("ImagePath"),
                                        CreatedOn = dataRow.Field<DateTime>("CreatedOn"),
                                        DescEn = dataRow.Field<string>("DescEn"),
                                        DescAr = dataRow.Field<string>("DescAr")
                                    }).ToList();
                                    obj.NewsList = hotellist;

                                }
                                else
                                {
                                    obj.NewsList = null;
                                }
                            }
                        }
                    }

                    else //if API execution is false
                    {
                        return View("Error");
                    }
                    if (TempData["obj"] != null)
                    {
                    }

                    string lang = VerifyUserSession();
                    //If any specific article is being read, load particular news view
                    if (name!=null)
                    {
                        if (lang == "En")
                        {
                            return View("ParticularNewsArticleView", obj);
                        }
                        else
                        {
                            return View("ParticularNewsArticleView_Ar", obj);
                        }
                    }

                    //Load following view only when news listing is being rendered
                    if (lang == "En")
                    {
                        return View(obj);
                    }
                    else
                    {
                        return View("News_Ar",obj);
                    }

                }
                catch (Exception ex)
                {
                    return View("Error");
                }
            }
        }

        public ActionResult ChangeLanguage()
        {
            String newURL = "/";
            if (Request.UrlReferrer != null)
            {
                String applPath = Request.Url.AbsolutePath;
                String refPath = Request.UrlReferrer.ToString();
                String basePath = Request.Url.ToString().Replace(applPath, "");
                String userPath = refPath.Replace(basePath, "");

                bool arabicUser = applPath.StartsWith("/ar/", true, System.Globalization.CultureInfo.InvariantCulture);

                newURL = basePath + (arabicUser == true ? userPath.Substring(3) : "/ar" + userPath);                
            }

            return Redirect(newURL);
            //RedirectToAction(PreviousPage);
        }
    }
}