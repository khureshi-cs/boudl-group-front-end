﻿using BoudlGroup.Handlers;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using System.Threading.Tasks;

namespace BoudlGroup.Controllers
{
    public class CharimenMessagesController : Controller
    {
        string img = System.Configuration.ConfigurationManager.AppSettings["ImageUrl"].ToString();

        // GET: CharimanMessage
        public ActionResult Index1()
        {
            return View();
        }
        public async Task<ActionResult> Index()
        {
            using (HttpClient client = new HttpClient())
            {
                CommonHeaders.setHeaders(client);
                try
                {
                    HomePageViewModel obj = new HomePageViewModel();
                    List<HomePageViewModel> boardmembersList = new List<HomePageViewModel>();

                    HttpResponseMessage responseMessage = await client.PostAsJsonAsync("api/BoardMembersAPI/NewGetBoardMembersList", obj);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        var responseData = responseMessage.Content.ReadAsStringAsync().Result;
                        var categories = JsonConvert.DeserializeObject<HomePageViewModel>(responseData);
                        var data = categories.datasetxml;

                        if (data != null)
                        {
                            var document = new XmlDocument();
                            document.LoadXml(data);
                            DataSet ds = new DataSet();
                            ds.ReadXml(new XmlNodeReader(document));
                            if (ds.Tables.Count > 0)
                            {

                                if (ds.Tables[1].Rows.Count > 0)
                                {
                                    boardmembersList = ds.Tables[1].AsEnumerable().Select(dataRow => new HomePageViewModel
                                    {
                                        Id = dataRow.Field<long>("Id"),
                                        NameEn = dataRow.Field<string>("NameEn"),
                                        NameAr = dataRow.Field<string>("NameAr"),
                                        DesignationEn = dataRow.Field<string>("DesignationEn"),
                                        DesignationAr = dataRow.Field<string>("DesignationAr"),
                                        MsgTitleEn = dataRow.Field<string>("MsgTitleEn"),
                                        MsgTitleAr = dataRow.Field<string>("MsgTitleAr"),
                                        MessageEn = dataRow.Field<string>("MessageEn"),
                                        MessageAr = dataRow.Field<string>("MessageAr"),
                                        IsBoardMember = Convert.ToBoolean(dataRow.Field<bool>("IsBoardMember")),
                                        HasMessage =Convert.ToBoolean(dataRow.Field<bool>("HasMessage")),
                                        ImagePath = img + "/" + dataRow.Field<string>("ImagePath"),
                                        CreatedOn = dataRow.Field<DateTime>("CreatedOn")
                                    }).ToList();
                                    obj.boardmemberslist = boardmembersList;
                                }
                                else
                                {
                                    obj.boardmemberslist = null;
                                }
                            }
                        }
                    }
                    else
                    {
                        return View("Error");

                    }
                    if (TempData["obj"] != null)
                    {
                        HomePageViewModel obj2 = new HomePageViewModel();
                        var editdata = TempData["obj"];
                        obj2 = (HomePageViewModel)editdata;
                        obj2.boardmemberslist = obj.boardmemberslist;
                        return View(obj2);
                    }
                    return View(obj);
                }
                catch (Exception ex)
                {
                    return View("Error");

                }
            }
        }
    }
}