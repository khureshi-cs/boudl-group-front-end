﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BoudlGroup.Controllers
{
    public class BaseController : Controller
    {
        // GET: Base
        //public ActionResult Index()
        //{
        //    return View();
        //}

        public string VerifyUserSession1()
        {
            if (Session["Language"] != null)
            {
                if (Session["Language"].ToString() == "En")
                {
                    return "En";
                }
                else
                {
                    return "Ar";
                }
            }
            else
            {
                Session["Language"] = "En";
                return "En";
            }
        }

    }
}