﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

namespace BoudlGroup
{
    public class HomePageViewModel
    {
        //BOARD MEMBERS DTO START
        public Int64 Id { get; set; }
        [Display(Name = "Name (English)")]
        [Required(ErrorMessage = "*Please Enter Name (English)", AllowEmptyStrings = false)]
        [RegularExpression(@"^[^<>.,?;:'()!~%\-_@#/*""]+$", ErrorMessage = "No special characters are allowed")]
        public string NameEn { get; set; }
        [Display(Name = "Name (Arabic)")]
        [Required(ErrorMessage = "*Please Enter Name (Arabic)", AllowEmptyStrings = false)]
        [RegularExpression(@"^[\u0621-\u064A\u0660-\u0669 ]+$", ErrorMessage = "Enter Arabic text only")]
        public string NameAr { get; set; }
        [Display(Name = "Designation (English)")]
        [Required(ErrorMessage = "*Please Enter Designation (English)", AllowEmptyStrings = false)]
        [RegularExpression(@"^[^<>.,?;:'()!~%\-_@#/*""]+$", ErrorMessage = "No special characters are allowed")]
        public string DesignationEn { get; set; }
        [Display(Name = "Designation (Arabic)")]
        [Required(ErrorMessage = "*Please Enter Designation (Arabic)", AllowEmptyStrings = false)]
        [RegularExpression(@"^[\u0621-\u064A\u0660-\u0669 ]+$", ErrorMessage = "Enter Arabic text only")]
        public string DesignationAr { get; set; }
        public string ImagePath { get; set; }
        public bool IsBoardMember { get; set; }
        public bool HasMessage { get; set; }

        [Display(Name = "Title (English)")]
        [Required(ErrorMessage = "*Please Enter Title (English)", AllowEmptyStrings = false)]
        [RegularExpression(@"^[^<>.,?;:'()!~%\-_@#/*""]+$", ErrorMessage = "No special characters are allowed")]
        public string MsgTitleEn { get; set; }
        [Display(Name = "Title (Arabic)")]
        [Required(ErrorMessage = "*Please Enter Tiltle (Arabic)", AllowEmptyStrings = false)]
        [RegularExpression(@"^[\u0621-\u064A\u0660-\u0669 ]+$", ErrorMessage = "Enter Arabic text only")]
        public string MsgTitleAr { get; set; }
        [Display(Name = "Message (English)")]
        [Required(ErrorMessage = "*Please Enter Message (English)", AllowEmptyStrings = false)]
        [RegularExpression(@"^[^<>.,?;:'()!~%\-_@#/*""]+$", ErrorMessage = "No special characters are allowed")]
        [AllowHtml]
        public string MessageEn { get; set; }
        [Display(Name = "Message (Arabic)")]
        [Required(ErrorMessage = "*Please Enter Message (Arabic)", AllowEmptyStrings = false)]
        [RegularExpression(@"^[\u0621-\u064A\u0660-\u0669 ]+$", ErrorMessage = "Enter Arabic text only")]
        [AllowHtml]
        public string MessageAr { get; set; }
        public int BoardMemberSortIndex { get; set; }
        public int MessagePageSortIndex { get; set; }

        public bool IsActive { get; set; }
        public DateTime CreatedOn { get; set; }
        public Int64 CreatedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public Int64 ModifiedBy { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime DeletedOn { get; set; }
        public Int64 DeletedBy { get; set; }
        public IEnumerable<HomePageViewModel> boardmemberslist { get; set; }
        public string NavigationUrl { get; set; }
        public int FlagId { get; set; }
        public string message { get; set; }

        public string datasetxml { get; set; }
        //public string imagepath { get; set; }
        public string ContentType { get; set; }
        public string filepath { get; set; }

        //BOARD MEMBERS DTO END



        //MANAGEHOTELS DTO START
      //  public Int64 Id { get; set; }
     //   [Required(ErrorMessage = "*Enter Hotel Name in English", AllowEmptyStrings = false)]
      //  [StringLength(20, ErrorMessage = "Hotel Name cannot be more than 20 characters")]
       // public string NameEn { get; set; }
      //  [Required(ErrorMessage = "*Enter Hotel Name in Arabic", AllowEmptyStrings = false)]
      //  [StringLength(20, ErrorMessage = "Hotel Name cannot be more than 20 characters")]
       // public string NameAr { get; set; }
        [Required(ErrorMessage = "*Enter Hotel Description in English", AllowEmptyStrings = false)]
        [AllowHtml]
        public string DescEn { get; set; }
        [Required(ErrorMessage = "*Enter Hotel Description in Arabic", AllowEmptyStrings = false)]
        [AllowHtml]
        public string DescAr { get; set; }
        [Required(ErrorMessage = "*Upload Hotel logo in English", AllowEmptyStrings = false)]
        public string LogoEn { get; set; }
        [Required(ErrorMessage = "*Upload Hotel logo in Arabic", AllowEmptyStrings = false)]
        public string LogoAr { get; set; }
        [Required(ErrorMessage = "*Select Hotel Type Group/Hotel", AllowEmptyStrings = false)]
        public int Type { get; set; }
        [Required(ErrorMessage = "*Select Country", AllowEmptyStrings = false)]
        public Int64 CountryId { get; set; }
        public Int64 OldCountryId { get; set; }
        public Int64 ParentId { get; set; }
     //   public bool IsActive { get; set; }
      //  public DateTime CreatedOn { get; set; }
      //  public int CreatedBy { get; set; }
     //   public DateTime ModifiedOn { get; set; }
      //  public int ModifiedBy { get; set; }
      //  public DateTime DeletedOn { get; set; }
     //   public int DeletedBy { get; set; }
      //  public bool IsDeleted { get; set; }
        public string CountryIds { get; set; }  
        public long[] aaa { get; set; }
        public IEnumerable<HomePageViewModel> HotelsList { get; set; }
        public IEnumerable<HomePageViewModel> NewsList { get; set; }

        public IEnumerable<HomePageViewModel> BranchList { get; set; }
        public IEnumerable<HomePageViewModel> SlideshowImages { get; set; }


        //  public string message { get; set; }
        //  public int FlagId { get; set; }
        //  public string datasetxml { get; set; }
        public Int64 hotelid { get; set; }
        public string url { get; set; }
        public string SourcePath { get; set; }
        public string Slug { get; set; }
        //MANAGEHOTELS DTO END
        public string param { get; set; }
        //CONTACT US FORM START
        [EmailAddress(ErrorMessage = "Invalid Email Address (ex: john@abc.com)")]
        public string Email { get; set; }
        public string Comment { get; set; }
        //CONTACT US FORM END

        public Int64 mediaTypeId { get; set; }

        public int slideshowType { get; set; }


    }
}