﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Configuration;

namespace BoudlGroup.Handlers
{
    public class CommonHeaders
    {
        public static void setHeaders(HttpClient client)
        {
            client.BaseAddress = new Uri(ConfigurationManager.AppSettings["apiurl"]);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }
    }
}