﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropertiesLayer
{
    public class Category
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public string CategoryName_Ar { get; set; }
        public bool? IsActive { get; set; }
        public int UserId { get; set; }
    }
}
